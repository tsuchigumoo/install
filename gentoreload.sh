#!/bin/bash

#classified by Low Level to high level

#Still have to compile dwm, ST and PluginInstall in vim if necessary

#SYSTEM

cp /home/gento/sync/install/conf/bashrc /home/gento/.bashrc


cp /home/gento/sync/install/conf/aliases.yml /home/gento/.config/glab-cli/aliases.yml
cp /home/gento/sync/install/conf/config.yml /home/gento/.config/glab-cli/config.yml

cp /home/gento/sync/install/conf/xmodmaprc /root/.Xmodmap
cp /home/gento/sync/install/conf/xmodmaprc /home/gento/.Xmodmap
cp /home/gento/sync/install/conf/Xdefaults /home/gento/.Xdefaults

cp /home/gento/sync/install/conf/xresources /root/.Xresources
cp /home/gento/sync/install/conf/xresources /home/gento/.Xresources

cp /home/gento/sync/install/conf/xresources /root/.Xdefaults

cp /home/gento/sync/install/conf/xsession /home/gento/.xsession
cp /home/gento/sync/install/conf/xinitrc /home/gento/.xinitrc

cp /home/gento/sync/install/conf/doas.conf /etc/doas.conf

# DWM

cp /home/gento/sync/install/dwm/dwm.c /home/gento/misc/dwm-6.2/dwm.c


rm home/gento/misc/dwm-6.2/config.h

cp /home/gento/sync/install/dwm/config.h /home/gento/misc/dwm-6.2/config.h
cp /home/gento/sync/install/dwm/gaplessgrid.c /home/gento/misc/dwm-6.2/gaplessgrid.c
cp /home/gento/sync/install/dwm/transient.c /home/gento/misc/dwm-6.2/transient.c

#ST

rm home/gento/misc/st-0.8.4/config.h

cp /home/gento/sync/install/st/* /home/gento/misc/st-0.8.4/
#Ranger

cp /home/gento/sync/install/ranger/rangerconf /home/gento/.config/ranger/rc.conf
cp /home/gento/sync/install/ranger/rifle.conf /home/gento/.config/ranger/rifle.conf
cp /home/gento/sync/install/ranger/scope.sh /home/gento/.config/ranger/scope.sh

#VM

cp /home/gento/sync/install/conf/vimrc /etc/vim/vimrc
cp /home/gento/sync/install/conf/pylintrc /home/gento/.pylintrc
cp /home/gento/sync/install/conf/vimrc /etc/vimrc

#greenclip
cp /home/gento/sync/install/conf/mytheme.rasi /home/gento/.config/rofi/mytheme.rasi

cp /home/gento/sync/install/conf/mytheme2.rasi /home/gento/.config/rofi/mytheme2.rasi

cp /home/gento/sync/install/neomutt/muttrc /home/gento/.config/mutt/muttrc

cp /home/gento/sync/install/neomutt/1-svrillaude.muttrc /home/gento/.config/mutt/accounts/1-svrillaude.muttrc


cp /home/gento/sync/install/neomutt/2-main.muttrc /home/gento/.config/mutt/accounts/2-main.muttrc

#### Redshift
cp /home/gento/sync/install/conf/redshift /home/gento/.config/redshift/redshift.conf

####Zathurarc


cp /home/gento/sync/install/conf/zathurarc /home/gento/.config/zathura/zathurarc

####Zathurarc

cp /home/gento/sync/install/conf/picom.conf /home/gento/.config/picom/picom.conf



####SCRIPTs
chmod +x /home/gento/sync/install/scripts/*
cp /home/gento/sync/install/scripts/yt.sh /usr/bin/yt
cp /home/gento/sync/install/scripts/4terms.sh /usr/bin/4terms.sh
cp /home/gento/sync/install/scripts/test.txt /home/gento/test.txt
cp /home/gento/sync/install/scripts/t.sh /usr/bin/t.sh
cp /home/gento/sync/install/scripts/e.sh /usr/bin/e.sh
cp /home/gento/sync/install/scripts/w.sh /usr/bin/w.sh
cp /home/gento/sync/install/scripts/wallp /usr/bin/wallp
cp /home/gento/sync/install/scripts/music.sh /usr/bin/music.sh


#####Rofi themes
cp /home/gento/sync/install/conf/mytheme.rasi /home/gento/.config/rofi/mytheme.rasi
cp /home/gento/sync/install/conf/copy.rasi /home/gento/.config/rofi/copy.rasi
cp /home/gento/sync/install/conf/mytheme3.rasi /home/gento/.config/rofi/mytheme3.rasi
cp /home/gento/sync/install/conf/mytheme4.rasi /home/gento/.config/rofi/mytheme4.rasi
cp /home/gento/sync/install/conf/mytheme5.rasi /home/gento/.config/rofi/mytheme5.rasi
cp /home/gento/sync/install/conf/mytheme6.rasi /home/gento/.config/rofi/mytheme6.rasi

# ZSHRC
cp /home/gento/sync/install/conf/zshrc /home/gento/.zshrc


## COMPILING DWM AND ST

cd /home/gento/misc/dwm-6.2/
sudo make clean install

cd /home/gentoo/misc/st-0.8.4/
sudo make clean install
