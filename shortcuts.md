Shortcuts

# Terminal (ST)
+ ctrl + j : zoom down
+ ctrl _+ k : zoom up
+ ctrl + l : open rofi+fzf to filter otput **lines** and copy it to clipboard
+ ctrl + L : open rofi+fzf to filter otput **lines URLS** and copy it to clipboard
+ ctrl + e : open vim to **edit** current term output

# WINDOW MANAGER (DWM)

- alt + Enter : Spawn a new terminal
- alt + j     : focus last window in the stack
- alt + k     : focus next window in the stack
- alt + f     : enter **fullscreen** mode
- alt + q     : **Quit** window
- alt + g     : Arranges windows to **grid** mode
- alt + b     : Hide or show **bar**
- alt + z     : swap window in stack
- alt + w     : Open **Web browser**
- alt + s     : Open keepassdx
- alt + t     : fuzzy find a directory and open it with ranger with **multiple terminals** , or open file directly
- alt + y     : Search **youtube** and play video using rofi and fzf
- alt + c     : Search **clipboard** using greenclip and fzf and rofi
- alt + d     : **dmenu** using rofi and fzf
- alt + r     : run **Redshift** for luminosity
- alt + v     : **Volume** up
- alt+shift+v : **Volume** Down
- alt+Shift+w : Change **Wallpaper**
# Firefox using vim mode

- f                : hover links
- F                : hover links to open in a new page
- g0               : go to first Tab
- g$               : go to last Tab
- ctrl + f then ESC: restore window focus after using ctrl + l in link area
