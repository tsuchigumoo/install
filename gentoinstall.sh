
mkdir /home/gento/misc
mkdir /home/gento/.config
mkdir /home/gento/.config/ranger
mkdir /home/gento/.config/mutt
mkdir /home/gento/.config/rofi
mkdir /home/gento/.config/redshift
mkdir /home/gento/.config/picom
mkdir /home/gento/.config/zathura
mkdir /home/gento/.vim/bundle

cd /home/gento/misc

wget https://dl.suckless.org/dwm/dwm-6.2.tar.gz

tar -xzf dwm-6.2.tar.gz

wget https://dl.suckless.org/st/st-0.8.4.tar.gz
git clone https://gitlab.com/CalcProgrammer1/OpenRGB#


tar -xzf st-0.8.4.tar.gz
wget https://github.com/erebe/greenclip/releases/download/3.3/greenclip

mv greenclip /usr/bin/greenclip
chown gento:gento /usr/bin/greenclip
chmod +x /usr/bin/greenclip

cp scripts/yt.sh /usr/bin/yt

git clone https://github.com/VundleVim/Vundle.vim.git /home/gento/.vim/bundle/Vundle.vim
git clone https://github.com/ycm-core/YouCompleteMe.git /home/gento/.vim/bundle/YouCompleteMe
cd ~/.vim/bundle/YouCompleteMe
python3 install.py --clangd-completer


cd /home/gento/.vim/bundle/YouCompleteMe

python3 install.py --go-completer --rust-completer  --clangd-completer


pip3 install pylint

#install doppler-utils for image previews in ranger

echo "startx" >> /etc/profile



# Need permissions
mkdir /root/.vim
mkdir /root/.vim/bundle
cp -r /home/gento/.vim/bundle/Vundle.vim /root/.vim/bundle/

command -v zsh |  tee -a /etc/shells
chsh -s /usr/bin/zsh gento

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
echo "source ${PWD}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> ${ZDOTDIR:-$HOME}/.zshrc
