
pacman -S --noconfirm archlinux-keyring

pacman -S --noconfirm xterm
pacman -S --noconfirm xorg-xinit
pacman -S --noconfirm xorg
pacman -S --noconfirm wget
pacman -S --noconfirm git
pacman -S --noconfirm cmake
pacman -S --noconfirm firefox

pacman -S --noconfirm libxft
pacman -S --noconfirm gcc
pacman -S --noconfirm libxinerama
pacman -S --noconfirm ranger
pacman -S --noconfirm rofi
pacman -S --noconfirm fzf
pacman -S --noconfirm redshift
pacman -S --noconfirm alsa
pacman -S --noconfirm pulseaudio
pacman -S --noconfirm pulseaudio-alsa
pacman -S  --noconfirm pavucontrol
pacman -S --noconfirm python-pip
pacman -S  --noconfirm zathura-pdf-mupdf
pacman -S  --noconfirm doas
pacman -S  --noconfirm doas
pacman -S  --noconfirm mpv
pacman -S  --noconfirm gcc
pacman -S  --noconfirm make
pacman -S  --noconfirm pkg-config
pacman -S  --noconfirm feh
pacman -S  --noconfirm screenfetch
pacman -S  --noconfirm alsa-utils
pacman -S  --noconfirm openvpn
pacman -S  --noconfirm  xsel
pacman -S  --noconfirm  xorg-xsetroot
pacman -S  --noconfirm  picom
pacman -S  --noconfirm  scrot
pacman -S --noconfirm keepassxc
pacman -S --noconfirm docker
pacman -S --noconfirm postgresql
pacman -S --noconfirm sof-firmware
pacman -S --noconfirm  dmenu
pacman -S --noconfirm  w3m
pacman -S --noconfirm  ueberzug
pacman -S --noconfirm  ffmpeg
pacman -S --noconfirm  ffmpegthumbnailer
pacman -S --noconfirm  ripgrep
pacman -S --noconfirm  xorg-xmodmap
pacman -S --noconfirm  xdotool
pacman -S --noconfirm firefox-tridactyl
pacman -S --noconfirm ttf-fira-code
pacman -S --noconfirm ddgr
pacman -S --noconfirm up
pacman -S --noconfirm zsh
pacman -S --noconfirm zsh-autosuggestions
pacman -S --noconfirm exa

## for rofi-search
pacman -S --noconfirm nodejs
pacman -S --noconfirm npm

## for Open rgb
pacman -S --noconfirm mbedtls

# writting

pacman -S --noconfirm libreoffice

#Add black arch repos

pacman -S  --noconfirm nmap
pacman -S  --noconfirm smbclient
pacman -S  --noconfirm exploitdb
pacman -S  --noconfirm go
pacman -S  --noconfirm nfs-utils
pacman -S  --noconfirm netcat
pacman -S  --noconfirm unzip
pacman -S  --noconfirm inetutils
pacman -S  --noconfirm mysql
pacman -S  --noconfirm hashcat
pacman -S  --noconfirm ruby

pacman -S  --noconfirm xorg-xhost
pacman -S  --noconfirm python-pipx

pipx ensurepath
pipx install crackmapexec

pacman -S  --noconfirm mingw-w64-gcc

pacman -S --noconfirm tftp-hpa

pacman -S --noconfirm tcpdump

pacman -S --noconfirm  burpsuite

mkdir /home/gento/oscptools
cd /home/gento/oscptools
go install github.com/OJ/gobuster/v3@latest
git clone https://github.com/3ndG4me/KaliLists
git clone https://github.com/danielmiessler/SecLists
git clone https://github.com/ffuf/ffuf
git clone https://github.com/jpillora/chisel
git clone https://github.com/liamim/shellshocker

pip3 install bloodhound

python3 -m pip install pipx
pipx ensurepath
pipx install crackmapexec
pipx install black
pipx install youtube-dl


go install github.com/ropnop/kerbrute@latest


# AUR
pacman -S  --noconfirm yay

##SQSH
pacman -S  --noconfirm yay
pacman -S --noconfirm texlive-most
pacman -S --noconfirm p7zip
pacman -S --noconfirm pandoc
yay sqsh

#ldapsearch
pacman -S --noconfirm openldap

# gem
gem install evil-winrm
PATH=$PATH:/home/gento/.local/share/gem/ruby/3.0.0/bin

# METASPLOIT
gem install bundler
bundle install


#IMPACKET
git clone https://github.com/SecureAuthCorp/impacket

# PSPY
git clone https://github.com/DominicBreuker/pspy

# QUEMU VIRTMANAGER
pacman -S --noconfirm libguestfs
pacman -S --noconfirm virt-manager qemu-desktop dnsmasq


# Semi manual steps
echo "1" | yay --no-confirm --removemake ani-cli
echo "1" | yay --no-confirm --removemake rofi-search
echo "1" | yay --no-confirm --removemake rofi-blocks-git


## MANUAL STEPS , Download manually VMWARE and run it

## then go to /usr/lib/polkit-gnome and run polkit-gnome-authentication-agent-1 and then  open a new terminal and run vmware
