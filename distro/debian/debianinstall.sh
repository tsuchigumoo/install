#!/bin/bash
cd
apt-get update  # To get the latest package lists
apt-get install compton
apt-get install vim -y
apt-get install firefox-esr -y

apt-get install vim-gtk -y

apt-get install ranger -y
apt-get install scrot -y
apt-get install screenfetch -y
apt-get install feh -y
apt-get install xsel -y
apt-get install rxvt-unicode -y
apt-getinstall  autocutsel -y
apt-get install python3 -y
apt-get install neomutt -y
apt-get install msmtp -y
apt-get install abook -y

apt-get install libudev-dev -y
apt-get install libxcb-composite0-dev -y
apt-get install build-essential cmake python3-dev -y


apt-get install zathura -y
apt-get install dmenu -y
apt-get install i3 -y

apt-get install pass -y
apt-get install isync -y
apt-get install python3-pip -y
apt-get install ddgr -y
apt-get install xorg -y

apt-get install xdm -y
apt-get install imagemagick -y


apt-get install bluetooth blueman -y

apt-get install firmware-misc-nonfree -y
#for polybar 


apt-get install pkg-config -y
apt-get install libcairo2-dev -y
apt-get install libasound2-dev -y
apt-get install libxcb-randr0-dev  -y
apt-get install xcb-proto -y
apt-get install xcb-proto -y

apt-get install libxcb-icccm4-dev -y
apt-get install libxcb-ewmh-dev -y
apt-get install libxcb-image0-dev -y
apt-get install libxcb-util0-dev -y
apt-get install python-xcbgen -y
apt-get install libjsoncpp-dev -y


sudo apt-get install cmake cmake-data libcairo2-dev libxcb1-dev libxcb-ewmh-dev libxcb-icccm4-dev libxcb-image0-dev libxcb-randr0-dev libxcb-util0-dev libxcb-xkb-dev pkg-config python-xcbgen xcb-proto libxcb-xrm-dev i3-wm libasound2-dev libmpdclient-dev libiw-dev libcurl4-openssl-dev libpulse-dev -y


git clone https://github.com/jaagr/polybar.git
mkdir ~/misc
mv polybar ~/misc/
cd ~/misc/polybar && ./build.sh


chmod 777 /home/install/info-pingrtt.sh
chmod 777 /home/install/info-ssh-sessions.sh


pip3 install setup-tools
pip3 install pywal
pip3 install --user pywal
pip3 install youtube-dl
pip3 install neovim

###i3 over everything else
update-alternatives --install /usr/bin/x-session-manager x-session-manager /usr/bin/i3 60

##NODEJS##
curl -sL https://deb.nodesource.com/setup_8.x | bash -
apt-get install -y nodejs

##LIVEDOWN##
npm install -g livedown
##VUNDLE##

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
cp /home/install/vimrc /etc/vim/vimrc
cp /home/install/bashrc .bashrc
cp /home/install/muttrc .config/mutt/
cp /home/install/color.muttrc .config/mutt/


# copy mutt wizard signature config
cd /root/
mkdir .mutt
cp /home/install/signature.sig /root/.mutt/mysig.sig 
    

##YOUCOMPLETEME##
cd 
cd .vim/bundle/YouCompleteMe
python3 install.py --all


cd 
cd .config
mkdir i3
cd
cp /home/install/i3 .config/i3/config
cp /home/install/xmodmaprc .xmodmaprc
## zoom urxvt ##
cp /home/install/polyconfig .config/polybar/config
cp /home/install/xresources .Xresources
mkdir .Xresources/ext

git clone https://github.com/majutsushi/urxvt-font-size  ~/.Xresources/ext
cp ~/.Xresources/ext/urxvt-font-size ~/.Xresources/ext

##Mutt wizard##
git clone https://github.com/LukeSmithxyz/mutt-wizard
mv mutt-wizard ~/misc/
cd ~/misc/mutt-wizard
sudo make install
mw add

cd
cd .vim/bundle/YouCompleteMe/

cd

python3 install.py --clang-completer


# GITHUB
cd 
cd .ssh
ssh-keygen -t rsa -C "sam.vrillaud@gmail.com"







# Rofi
apt-get install rofi -y

wget https://github.com/erebe/greenclip/releases/download/3.2/greenclip
mv greenclip ~/misc/
cp ~/misc/greenclip /usr/local/bin
chmod +x /usr/local/bin/greenclip
cd

cp mytheme .config/rofi/mytheme.rasi
cp mytheme2 .config/rofi/mytheme.rasi2


#gdrive



git clone https://github.com/ckb-next/ckb-next.git
mv ckb-next ~/misc/
cd ~/misc/ckb-next
./quickinstall





#ytop beautifull command line top
apt-get install cargo -y
cargo install -f --git https://github.com/cjbassi/ytop ytop


# qr file transfers
pip3 install qr-filetransfer[extras]

 # view reddit in command line
 pip3 install rtv

apt-get install npm -y

npm install -g mapscii

apt-get install ctags -y


apt-get install beets -y



mkdir ~.config/rofi

### i3 gaps, no package on deb deb
git clone https://www.github.com/maestrogerardo/i3-gaps-deb
