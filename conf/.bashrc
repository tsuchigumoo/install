cd
clear
export PATH="${PATH}:${HOME}/.local/bin/"
export BROWSER=w3m
export PATH="${PATH}:${HOME}./root/misc/geckodriver"

[[ $- != *i* ]] && return

# ALIASES
## Productivity
PS1='[\u@\h \W]\$ '
alias sudo='doas'
alias p='pacman -S'
alias s='sudo -s'
alias r='ranger --choosedir=$HOME/.rangerdir; LASTDIR=`cat $HOME/.rangerdir`; cd "$LASTDIR"'
alias x=' xclip -selection clipboard'
alias z='zathura'
alias ss='scrot -d 5 -c'
alias c='clear'
alias d='value=$(</sys/class/backlight/intel_backlight/brightness);min=1000;
echo $((value-min)) > /sys/class/backlight/intel_backlight/brightness;echo $value'
alias sr='systemctl restart'
alias sp='systemctl stop'
alias b='value=$(</sys/class/backlight/intel_backlight/brightness);min=1000;
echo $((value+min)) > /sys/class/backlight/intel_backlight/brightness;echo $value'
set -o vi
export EDITOR=vim
alias v='vim'
alias nm='neomutt'

#WEB
alias w='ddgr $1'

# Pentesting

## Quick nmap scans
alias nsimple='nmap -sC -sV -T4 -Pn $IP > simple.txt'
alias nhardcore='sudo nmap -sSVC --script=vuln* -Pn -p- -T5 -oA scan $IP > hardcore.txt'
alias nudp='sudo nmap -sU -sV -sC -n -F -T4 $IP > udp.txt'
alias nsmb='nmap -p 445 --script=smb-enum-shares.nse,smb-enum-users.nse,smb-vuln*  $IP > smb.txt'
alias nnfs='nmap -p 111 --script=nfs-ls,nfs-statfs,nfs-showmount $IP'
alias nsmtp='nmap -p 25 --script=smtp-enum-users,smtp-open-relay,smtp-commands $IP'

## python server port 80
alias serv='sudo python3 -m http.server'

# searchsploit
alias sp='searchsploit'

# Doc

alias cracking='vim /home/gento/sync/oscp/cracking/hash.txt '
alias enum='vim  /home/gento/sync/oscp/enum.txt'
alias exploits='vim  /home/gento/sync/oscp/exploits.txt'
alias revshells='vim  /home/gento/sync/oscp/revshells/revshells.txt'
alias revshells='vim  /home/gento/sync/oscp/revshells/revshells.txt'
alias privesc='vim  /home/gento/sync/oscp/privesc/methodo.txt'
alias pullandpush='vim /home/gento/sync/oscp/windows/pullandpush.txt'

##########COLORS#######
alias ls='ls --color=auto'
alias dir='dir --color=auto'

alias grep='grep --color=auto'

#ee /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
    *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
PS1='\[\033[1;36m\]\u\[\033[1;31m\]@\[\033[1;32m\]\h:\[\033[1;35m\]\w\[\033[1;31m\]\$\[\033[0m\] '
