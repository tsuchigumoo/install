# Create gento user:
useradd -m gento
passwd gento

# DEFAULT CONFIG

## Install gento user stuff:

su gento

sudo chmod +x gentoinstall.sh

###
- Issues:
Fix "invalid or corrupted package (PGP signature)" error in Arch Linux
sudo pacman -S archlinux-keyring
###

## Reload gento conf:

sudo chmod +x gentoreload.sh

### Distro specific:
# Arch:

cd distro/arch
chmod +x pacman.sh
sudo ./pacman.sh

# gentoo:

# MANUAL STEPS

## vim
:PluginInstall

##Put in bash_profile
export LC_ALL="C"

## FIREFOX
<!-- install vim vixen -->
install trydactyl
install ublockorigin
activate all filters for annoyance in ublock
block consent.google.com  cookies
delete cookies when firefox exits

## docker
Add gento to the docker group
