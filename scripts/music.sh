#!/bin/bash
pathy=$(find /home/gento/beets | rofi -dmenu -i -sort -sorting-method fzf -theme /home/gento/.config/rofi/mytheme6.rasi)
echo $pathy
path=${pathy:2}
pathfull=/home/gento/$path
echo $pathfull
# rofi -show run -theme /home/gento/.config/rofi/mytheme.rasi


for i in $pathfull ; do
    if [[ $i == *.* ]] ; then
		echo "dot found"
        dot=true
    fi
done

filename=${pathfull##*/}
echo $filename

if [[ "$filename" =~ \ |\' ]]
then
   echo "Matches"
    filename=$filename
else
   echo "No matches"
fi


killall mpv

DIR=$(dirname "${pathfull}") ;
pathfull=$DIR/$filename
echo $pathfull
echo $filename

    if [[ $dot = true ]] ; then
    echo "dotty"
        wallp "$pathy"
    elif [[ -z $pathy ]]; then
        echo "do nothing"
    else
        echo "folder"
        cd $pathy
        wallp
    fi

killall wallp

