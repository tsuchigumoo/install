##for additional ddgr options see "ddgr --help"
#export DDG_ARGS='["-n", 5]'
#export ROFI_SEARCH='ddgr'
#export ROFI_SEARCH_TIMEOUT=200
#export ROFI_SEARCH_DEBUG=1
#export ROFI_SEARCH_CMD='firefox -new-window $URL'

#rofi -modi blocks -blocks-wrap rofi-search -show blocks \
#-lines 4 -eh 4 -kb-custom 1 -theme /home/gento/.config/rofi/mytheme5.rasi



#!/usr/bin/bash


main() {

    query=$( (echo ) | rofi  -dmenu -matching fuzzy -theme /home/gento/.config/rofi/mytheme5.rasi -p "Firefox > " )

    if [[ $query = http* ]]; then
      firefox -new-window "$query"
      echo $query > /home/gento/tety.txt
    elif [[ -n "$query" ]]; then
      url="https://www.google.com/search?q="$query
      firefox -new-window "$url"

    else
      exit
    fi

}

main

exit 0

