#!/bin/sh
regex='(((http|https|ftp|gopher)|mailto)[.:][^ >"\t]*|www\.[-a-z0-9.]+)[^ .,;\t>">\):]'
grep -Po "$regex" | rofi -dmenu -i -sort -sorting-method fzf -theme /home/gento/.config/rofi/mytheme.rasi  | xclip -selection clipboard

