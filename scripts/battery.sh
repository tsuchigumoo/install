#!/bin/bash

battery=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep percentage)

while true; 
do 
    xsetroot -name "$battery"; 
    sleep 10; 
done

